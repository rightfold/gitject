:- module hprop.

:- interface.
:- import_module list.

:- type hprop == list({string, string}).

:- pred parse(string::in, hprop::out) is semidet.

:- pred serialize(hprop::in, string::out) is det.

:- implementation.
:- import_module bool, string.

parse(Text, Hprop) :-
    Lines = string.split_at_char('\n', Text),
    parse(Lines, [], Hprop).

:- pred parse(list(string)::in, hprop::in, hprop::out) is semidet.
parse([], Acc, Hprop) :-
    Hprop = list.reverse(Acc).
parse([Line | Lines], Acc, Hprop) :-
    ( if Line = ""
      then parse(Lines, Acc, Hprop)
      else [Key, Value] = string.split_at_char(':', Line),
           parse(Lines, Key, [Value], Acc, Hprop)
    ).

:- pred parse(list(string)::in, string::in, list(string)::in, hprop::in, hprop::out) is semidet.
parse([Line | Lines], Key, Value_lines, Acc, Hprop) :-
    ( if string.prefix(Line, " ")
      then parse(Lines, Key, [Line | Value_lines], Acc, Hprop)
      else Acc2 = [{Key, string.join_list("\n", list.reverse(Value_lines))} | Acc],
           parse([Line | Lines], Acc2, Hprop)
    ).

serialize([], Text) :-
    Text = "".
serialize([{Key, Value} | Hprop], Text) :-
    Value_lines = list.map(func(I) = " " ++ I, string.split_at_char('\n', Value)),
    Prefix = Key ++ ":" ++ string.join_list("\n", Value_lines) ++ "\n",
    serialize(Hprop, Suffix),
    Text = Prefix ++ Suffix.
