:- module bug.

:- interface.
:- import_module hprop.

:- type id ---> id(string).

:- type status ---> open; closed.

:- type bug --->
    bug(
        id          :: id,
        status      :: status,
        summary     :: string,
        description :: string
    ).

:- pred hprop(bug, hprop).
:- mode hprop(in, out) is det.
:- mode hprop(out, in) is semidet.

:- implementation.
:- import_module list.

hprop(bug(id(ID), Status, Summary, Description),
      [{"id", ID},
       {"status", StatusString},
       {"summary", Summary},
       {"description", Description}]) :-
    ( Status = open, StatusString = "open"
    ; Status = closed, StatusString = "closed"
    ).
