:- module gitject.

:- interface.
:- import_module io.

:- pred main(io::di, io::uo) is det.

:- implementation.
:- import_module bug, hprop, list, string.

:- pred parse_example(string::in, io::di, io::uo) is det.
:- pred serialize_example(hprop::in, io::di, io::uo) is det.

parse_example(Text, !IO) :-
    ( if hprop.parse(Text, Hprop)
      then io.print(Hprop, !IO),
           io.write_string("\n", !IO)
      else io.write_string("oops!\n", !IO)
    ).

serialize_example(Hprop, !IO) :-
    hprop.serialize(Hprop, Text),
    io.write_string("---------- ", !IO),
    io.print(Hprop, !IO),
    io.write_string("\n", !IO),
    io.write_string(Text, !IO).

main(!IO) :-
    parse_example("", !IO),
    parse_example("k: v\n", !IO),
    parse_example("k: v\nl: w\n", !IO),
    parse_example("k: v\nl: w\nm: x\n", !IO),
    parse_example("k: v\nl: w\nm:\n x\n y\n", !IO),
    parse_example("k: v\nl: w\nm:\n x\n y\nn: y\n", !IO),

    serialize_example([], !IO),
    serialize_example([{"k", "v"}], !IO),
    serialize_example([{"k", "v"}, {"l", "w"}], !IO),
    serialize_example([{"k", "v"}, {"l", "w"}, {"m", "x\ny"}], !IO),
    serialize_example([{"k", "v"}, {"l", "w"}, {"m", "x\ny"}, {"n", "y"}], !IO).
